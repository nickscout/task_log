package com.epam.nickscout.log;

import org.telegram.telegrambots.bots.DefaultBotOptions;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Chat;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TelegramLoggerBot extends TelegramLongPollingBot {

    private final String TOKEN = "899514330:AAHUOCSxAvWbsWGxI8Ug9N-o5OpqRZW6H6I";
    private final String BOTUSERNAME = "NickScoutLoggerBot";
    private Set<Chat> chats;

    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        if (message.hasText() && message.getText() == "/start") {
            chats.add(message.getChat());
            SendMessage response = new SendMessage();
            response.setText("Thanks for subscribing! I will now send you logs");
            response.setChatId(message.getChat().getId());
            try {
                execute(response);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }
    }

    public void onUpdatesReceived(final List<Update> updates) {
        updates.forEach(this::onUpdateReceived);
    }

    public String getBotUsername() {
        return BOTUSERNAME;
    }

    public String getBotToken() {
        return TOKEN;
    }

    public TelegramLoggerBot() {
        this.chats = new HashSet<Chat>();
    }

    public void log(String string) {
        SendMessage message = new SendMessage();
        message.setText(string);
        chats.forEach(chat -> {
            message.setChatId(chat.getId());
            try {
                execute(message);
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        });
    }
}
