package com.epam.nickscout.log;

import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;

import java.io.Serializable;

@Plugin(
        name = "TelegramAppender",
        category = "Core",
        elementType = Appender.ELEMENT_TYPE,
        printObject = true
)
public class TelegramAppender extends AbstractAppender {

    private TelegramLoggerBot loggerBot;

    protected TelegramAppender(String name, Filter filter, Layout<? extends Serializable> layout) {
        super(name, filter, layout);
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        loggerBot = new TelegramLoggerBot();
        try {
            telegramBotsApi.registerBot(loggerBot);
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }

    protected TelegramAppender(String name, Filter filter, Layout<? extends Serializable> layout, boolean ignoreExceptions) {
        super(name, filter, layout, ignoreExceptions);
        ApiContextInitializer.init();
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
        try {
            telegramBotsApi.registerBot(new TelegramLoggerBot());
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }
    }

    public void append(LogEvent logEvent) {
        loggerBot.log(new String(getLayout().toByteArray(logEvent)));
    }

    @PluginFactory
    public static TelegramAppender createAppender(
            @PluginAttribute("name") String name,
            @PluginElement("Layout") Layout<? extends Serializable> layout,
            @PluginElement("Filter") final Filter filter,
            @PluginAttribute("otherAttribute") String otherAttribute) {
        if (layout == null) {
            layout = PatternLayout.createDefaultLayout();
        }
        return new TelegramAppender(name,filter,layout,true);
    }
}
